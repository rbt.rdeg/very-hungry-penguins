(** Block module *)

(** A block *)
type t =
    Empty
  | Fish of int
  | Penguin of Player.t
